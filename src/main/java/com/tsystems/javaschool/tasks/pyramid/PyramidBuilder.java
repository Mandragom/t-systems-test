package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */

    private int rows, cols;

    public PyramidBuilder() {
        this.rows = 0;
        this.cols = 0;
    }
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        try {
            Collections.sort(inputNumbers);
        } catch (NullPointerException | OutOfMemoryError exp) {
            throw new CannotBuildPyramidException("List contains NULL's values");
        }
        if (!checkCount(inputNumbers.size())) {
            throw new CannotBuildPyramidException("Illegal count of elements");
        }
        // TODO : Implement your solution here
        int[][] result = new int[rows][cols];
        // Filling result by zero
        for (int i = 0 ; i < rows;i++) {
            for (int j = 0 ; j < cols; j++) {
                result[i][j] = 0;
            }
        }
        int center = rows - 1;
        for (int i = 0, k = 0; i < rows; i++) {
            if (k == 0) {
                result[0][center] = inputNumbers.get(k);
                k++;
            } else {
                for (int x = center - i ; x <= center + i;x += 2 ) {
                    result[i][x] = inputNumbers.get(k);
                    k++;
                }
            }
        }
        return result;
    }
    private boolean checkCount(int count) {
        if (count < 1) throw new CannotBuildPyramidException("Illegal count of elements");
        for (int i = 0, k = 1; true ; k++) {
            i+=k;
            if (i == count) {
                this.rows = k;
                this.cols = k * 2 - 1;
                return true;
            }
            else if (i > count ) return false;
        }
    }

}
