package com.tsystems.javaschool.tasks.calculator;

import java.util.EmptyStackException;
import java.util.Stack;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    private Stack<Double> stackRPN;

    public Calculator() {
        this.stackRPN = new Stack<>();
    }
    public String evaluate(String statement) {
        try {
            String postfix = parseToRPN(statement);
            String[] arguments = postfix.split(" ");
            for (String arg : arguments) {
                if (operand(arg)) {
                    stackRPN.push(Double.valueOf(arg));
                } else {
                    Double arg1 = stackRPN.pop(), arg2 = stackRPN.pop();
                    stackRPN.push(operate(arg, arg2, arg1));
                }
            }
            Double result = stackRPN.pop();
            if (stackRPN.empty()) {
                if (result - result.intValue() == 0) return String.valueOf(result.intValue());
                else return String.valueOf(result);
            } else return null;
        } catch (IllegalArgumentException | EmptyStackException | NullPointerException e) {
            return null;
        }
    }

    public Double operate(String operator, double arg1, double arg2) {
        switch (operator) {
            case "+": return arg1 + arg2;
            case "-": return arg1 - arg2;
            case "*": return arg1 * arg2;
            case "/": if (arg2 != 0) return arg1 / arg2; else throw new IllegalArgumentException("Divided by Zero") ;
            default: return null;
        }
    }

    public Double operate(String operator, String arg1, String arg2) {
        return operate(operator, Double.parseDouble(arg2), Double.parseDouble(arg2));
    }
    private String parseToRPN(String prefixString) {
        Stack<Character> operators = new Stack<>();
        StringBuilder result = new StringBuilder();
        boolean lastDigit = true;
        for (char c : prefixString.toCharArray()) {
            if (c >= '0' && c <= '9' || c == '.') {
                lastDigit = true;
                result.append(c);
            }
            else if (c == '(') {
                operators.push(c);
            }
            else if (c == ')') {
                while ( (operators.peek()) != '(') {
                    result.append(" ");
                    result.append(operators.pop());
                }
                operators.pop(); // Delete '(' from stack
            } else {
                if (lastDigit) {
                    result.append(" ");
                    lastDigit = false;
                }
                while (!operators.empty() && (priority(c) <= priority(operators.peek()))) {
                    result.append(operators.pop());
                    result.append(" ");
                }
                operators.push(c);
            }
        }
        while ( !operators.empty() ) {
            result.append(" ").append(operators.pop());
        }
        return result.toString();
    }

    private int priority(char operator) {
        if (operator == '*' || operator == '/') return 2;
        else if (operator == '-' || operator == '+') return 1;
        else if (operator == '(' || operator == ')') return 0;
        else operatorException(String.valueOf(operator));
        return Integer.MAX_VALUE;
    }
    private int priority(String operator) {
        if (operator.length() != 1) operatorException(operator);
        return priority(operator.charAt(0));
    }
    private void operatorException(String operator) {
        throw new IllegalArgumentException("Unsupported operator: " + operator + ". Unknown priority.");
    }

    private boolean operand(String value) {
        return value.matches("(-?\\d+\\.?\\d+)|(-?\\d+)");
    }

}
