package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if (x == null || y == null) throw new IllegalArgumentException("Null pointer on one of list");
        boolean[] exist = new boolean[x.size()];
        int pos = 0;
        for (Object o1 : x) {
            for (int i = pos; i < y.size(); i++) {
                Object o2 = y.get(i);
                if (o1.equals(o2)) {
                    pos = i;
                    exist[x.indexOf(o1)] = true;
                    break;
                }
            }
        }
        boolean result = true;
        for (boolean b : exist) {
            result&=b;
        } return result;
    }
}
